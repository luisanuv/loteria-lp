#ifndef QUINA_H
#define QUINA_H
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <iostream>
#include <string>
#include <vector>
#include "loteria.h"
using namespace std;

class Quina{

public:
    Quina();

    int n,num_apuestas,tamanio_apuesta;
    float* funcion();
    float funcion2();
    void juego_quina();
    float total_acumulado();


};

#endif // QUINA_H
