TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    loteria.cpp \
    quina.cpp \
    lotogol.cpp

HEADERS += \
    loteria.h \
    quina.h \
    lotogol.h

